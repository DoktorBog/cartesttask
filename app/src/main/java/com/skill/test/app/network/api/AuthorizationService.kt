package com.skill.test.app.network.api

import com.skill.test.app.network.models.LoginRequest
import com.skill.test.app.network.models.LoginResponce
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthorizationService {

    @POST("auth/login")
    suspend fun login(@Body loginRequest: LoginRequest): Response<LoginResponce>

    @POST("auth/logout")
    suspend fun logout(): Response<Nothing>

}