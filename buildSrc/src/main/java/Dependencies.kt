object App {
    val id = "com.skill.test.app"

    val minSdkVersion = 22
    val targetSdkVersion = 29
    val compileSdkVersion = targetSdkVersion
}

object Versions {

    val codeVersion = 1
    val codeVersionName = "1.0.0"

    val supportLib = "27.0.2"

    object Android {
        val gradle = "3.5.0"
    }

    object AndroidX {
        val core = "1.0.2"
        val testRunner =  "1.1.1"
        val espressoCore = "3.1.1"
        val constraintlayout = "1.1.3"
        val viewModel = "2.2.0-alpha05"
    }

    object Jetbrains {
        val kotlin = "1.3.41"
    }

    object Squareup {
        val retrofit = "2.6.2"
        val okHttp = "4.2.0"
    }

    object Google {
        val dagger = "2.24"
        val material = "1.1.0-alpha10"
        val gson = "2.6.2"
    }

    object Junit {
        val junit = "4.12"
    }
}

object Libs {

    val androidXCore = "androidx.core:core-ktx:${Versions.AndroidX.core}"

    object Design {
        val material = "com.google.android.material:material:${Versions.Google.material}"
        val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.AndroidX.constraintlayout}"
    }

    object Network {
        val retrofit = "com.squareup.retrofit2:retrofit:${Versions.Squareup.retrofit}"
        val okHttp = "com.squareup.okhttp3:okhttp:${Versions.Squareup.okHttp}"
        val okHttpLogging = "com.squareup.okhttp3:logging-interceptor:${Versions.Squareup.okHttp}"
        val gson = "com.google.code.gson:gson:${Versions.Google.gson}"
        val gsonConverter = "com.squareup.retrofit2:converter-gson:${Versions.Google.gson}"
    }

    object Lifecycle {
        val viewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.AndroidX.viewModel}"
    }

    object Dagger {
        val core = "com.google.dagger:dagger-android:${Versions.Google.dagger}"
        val compiler = "com.google.dagger:dagger-compiler:${Versions.Google.dagger}"
        val android = "com.google.dagger:dagger-android-support:${Versions.Google.dagger}"
        val processor = "com.google.dagger:dagger-android-processor:${Versions.Google.dagger}"
    }

    val junit = "junit:junit:${Versions.Junit.junit}"
    val testRunner = "androidx.test:runner:${Versions.AndroidX.testRunner}"
    val espressoCore = "androidx.test.espresso:espresso-core:${Versions.AndroidX.espressoCore}"
}