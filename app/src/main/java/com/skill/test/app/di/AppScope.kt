package com.skill.test.app.di

import javax.inject.Scope

@Retention(AnnotationRetention.RUNTIME)
@Scope
annotation class AppScope