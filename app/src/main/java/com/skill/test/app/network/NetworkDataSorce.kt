package com.skill.test.app.network

import com.skill.test.app.network.api.CarAppService

class NetworkDataSorce constructor(
    private val service: CarAppService
) {

    suspend fun profile() = service.profile()

    suspend fun loadCars() = service.cars()

    suspend fun loadCarsStates() = service.carInfo()

}