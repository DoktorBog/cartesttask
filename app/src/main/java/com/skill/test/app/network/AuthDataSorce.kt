package com.skill.test.app.network

import com.skill.test.app.network.api.AuthorizationService
import com.skill.test.app.network.models.LoginRequest

class AuthDataSorce constructor(
    private val service: AuthorizationService
) {

    suspend fun login(nickname: String, password: String) = service.login(LoginRequest(nickname, password))

    suspend fun logout() = service.logout()
}