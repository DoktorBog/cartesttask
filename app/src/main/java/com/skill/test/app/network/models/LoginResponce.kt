package com.skill.test.app.network.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LoginResponce(
    @SerializedName("token")
    val token: String
): Parcelable