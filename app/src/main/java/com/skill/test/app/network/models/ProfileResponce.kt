package com.skill.test.app.network.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProfileResponce (

    @SerializedName("id")
    val id: String,

    @SerializedName("name")
    val name: String,

    @SerializedName("email")
    val email: String,

    @SerializedName("settings")
    val settings: UserSettings,

    @SerializedName("token")
    val mobileNumber: String,

    @SerializedName("userRole")
    val userRole: String
): Parcelable

@Parcelize
data class UserSettings(
    val overviewMapPosition: String
): Parcelable

/*
{
    "id": "c13b6be6-3729-4c75-8551-5687a1da1db0",
    "parent": "00000000-0000-0000-0000-000000000000",
    "company": "fd963b2c-10db-4004-a647-bf621fb1ef97",
    "name": "Mobile Test",
    "accessLevel": "user, admin",
    "email": "mobile.test@mobiliuz.com",
    "settings": {
    "overviewMapPosition": "30.648574999999997,50.43087799999998,18",
    "timezone": null,
    "notificationSettings": null
},
    "mobileNumber": "0000000000",
    "telegramUsername": "0000000000",
    "userRole": "legalEntityAdmin"
}*/