package com.skill.test.app.network

import okhttp3.Interceptor
import okhttp3.logging.HttpLoggingInterceptor

class HttpLogging private constructor(){
    companion object {
        val loggingInterceptor: Interceptor = HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }
    }
}