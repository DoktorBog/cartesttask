package com.skill.test.app.di

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.skill.test.app.network.*
import com.skill.test.app.network.api.AuthorizationService
import com.skill.test.app.network.api.CarAppService
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named

@Module
class NetworkModule {

    @Provides
    @AppScope
    fun provideAuthDataSorce(service: AuthorizationService)
            = AuthDataSorce(service)

    @Provides
    @AppScope
    fun provideNetworkDataSorce(service: CarAppService)
            = NetworkDataSorce(service)

    @Provides
    @AppScope
    fun provideAuthService(@Named("Retrofit.NotAuthorized") retrofit: Retrofit)
            = retrofit.create(AuthorizationService::class.java)

    @Provides
    @AppScope
    fun provideCarService(@Named("Retrofit.Authorized") retrofit: Retrofit)
            = retrofit.create(CarAppService::class.java)

    @Provides
    @AppScope
    @Named("Retrofit.Authorized")
    fun provideAuthorizedRetrofit(@Named("OkHttp.Secure") okHttpClient: OkHttpClient, gson: Gson): Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .client(okHttpClient).build()

    @Provides
    @AppScope
    @Named("Retrofit.NotAuthorized")
    fun provideNotAuthorizedRetrofit(@Named("OkHttp") okHttpClient: OkHttpClient, gson: Gson): Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .client(okHttpClient).build()

    @Provides
    @AppScope
    @Named("OkHttp.Secure")
    fun provideSecureOkHttp(context: Context, settings: AppSettings): OkHttpClient  = OkHttpClient.Builder()
        .cache(Cache(context.cacheDir, 1024*1024))
        .addInterceptor(TokenInterceptor(settings))
        .addInterceptor(HttpLogging.loggingInterceptor)
        .build()

    @Provides
    @AppScope
    @Named("OkHttp")
    fun provideOkHttp(): OkHttpClient  = OkHttpClient.Builder()
        .addInterceptor(HttpLogging.loggingInterceptor)
        .build()

    @Provides
    @AppScope
    fun provideGson(): Gson = GsonBuilder().setLenient().create()

    companion object {
        const val BASE_URL = "https://devapi.mobiliuz.com/"
    }
}