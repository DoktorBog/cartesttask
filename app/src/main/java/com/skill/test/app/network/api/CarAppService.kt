package com.skill.test.app.network.api

import com.skill.test.app.network.models.CarsDetailsResponce
import com.skill.test.app.network.models.CarsResponce
import com.skill.test.app.network.models.LoginResponce
import com.skill.test.app.network.models.ProfileResponce
import retrofit2.Response
import retrofit2.http.GET

interface CarAppService {

    @GET("profile")
    suspend fun profile(): Response<ProfileResponce>

    @GET("cars")
    suspend fun cars(): Response<CarsResponce>

    @GET("cars/states")
    suspend fun carInfo(): Response<CarsDetailsResponce>

}