package com.skill.test.app.network.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class CarsResponce(
    @SerializedName("id")
    val id: String,

    @SerializedName("imei")
    val imei: String,

    @SerializedName("company")
    val company: String,

    @SerializedName("status")
    val status: String,

    @SerializedName("nickname")
    val nickname: String,

    @SerializedName("vin")
    val vin: String,

    @SerializedName("model")
    val model: String,

    @SerializedName("manufacturer")
    val manufacturer: String
    //val settings: SettingsBean
): Parcelable

/*
data class SettingsBean(
    val general: GeneralBean,
    val engine: EngineBean,
    val tracker: TrackerBean,
    val fuelCalibration: FuelCalibrationBean,
    val maintenanceSettings: MaintenanceSettingsBean,
    var isRemoteControlEnabled: Boolean = false
)*/
