package com.skill.test.app.di

import android.app.Application
import com.skill.test.app.App
import dagger.Component
import dagger.BindsInstance
import dagger.android.AndroidInjectionModule

@Component(modules = [AndroidInjectionModule::class, ActivityModule::class, NetworkModule::class, AppModule::class])
@AppScope
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }
    fun inject(app: App)
}
