package com.skill.test.app.network

import okhttp3.Interceptor
import okhttp3.Response

class TokenInterceptor constructor(
    private val settings: AppSettings
): Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        return if (settings.token != null) {
            val request = originalRequest.newBuilder()
                .header("Authorization", "Token ${settings.token}")
            chain.proceed(request.build())
        } else {
            chain.proceed(originalRequest)
        }
    }
}