package com.skill.test.app.network

import android.content.Context
import android.content.SharedPreferences
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppSettings @Inject constructor(context: Context){

    private val prefs: SharedPreferences = context.getSharedPreferences("app_settings", Context.MODE_PRIVATE)

    private var _token: String?

    init {
        _token = prefs.getString(KEY_TOKEN, null)
    }

    var token
        get() = _token
        set(value) {
            _token = value
            prefs.edit().putString(KEY_TOKEN, value).apply()
        }

    companion object {
        const val KEY_TOKEN = "token"
    }
}