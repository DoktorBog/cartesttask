package com.skill.test.app.di

import android.content.Context
import com.skill.test.app.network.AppSettings
import dagger.Module
import dagger.Provides

@Module
class AppModule {

    @Provides
    @AppScope
    fun provideAppSettings(context: Context) = AppSettings(context)
}