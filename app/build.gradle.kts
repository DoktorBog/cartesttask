import org.jetbrains.kotlin.config.KotlinCompilerVersion

plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("android.extensions")
    kotlin("kapt")
}

android {
    compileSdkVersion(App.compileSdkVersion)
        defaultConfig {
            applicationId = App.id
            minSdkVersion(App.minSdkVersion)
            targetSdkVersion(App.targetSdkVersion)
            versionCode = Versions.codeVersion
            versionName = Versions.codeVersionName
            testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        }

    dataBinding {
        isEnabled = true
    }

    androidExtensions {
        isExperimental = true
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
}


dependencies {
    implementation(Libs.Design.material)
    implementation(Libs.Design.constraintLayout)

    implementation(Libs.Lifecycle.viewModel)

    implementation(Libs.Network.retrofit)
    implementation(Libs.Network.okHttp)
    implementation(Libs.Network.okHttpLogging)
    implementation(Libs.Network.gson)
    implementation(Libs.Network.gsonConverter)

    kapt(Libs.Dagger.compiler)
    kapt(Libs.Dagger.processor)
    implementation(Libs.Dagger.core)
    implementation(Libs.Dagger.android)

    implementation(Libs.androidXCore)
    implementation(kotlin("stdlib-jdk7", KotlinCompilerVersion.VERSION))
    testImplementation(Libs.junit)
    androidTestImplementation(Libs.testRunner)
    androidTestImplementation(Libs.espressoCore)
}
